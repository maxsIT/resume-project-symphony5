<?php

namespace App\Service;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file, ?string $fileName) : ?string
    {
        if ($fileName != null)
        {
            $this->delete($fileName);
        }

        $fileName = uniqid().'.'.$file->guessExtension();
        try {
            $file->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $e) {
            return null;
        }

        return $fileName;
    }

    public function delete(?string $fileName) : bool
    {
        if ($fileName == null) {
            return false;
        }

        try {
            $fileSystem = new Filesystem();
            $fileSystem->remove($this->getTargetDirectory().'/'.$fileName);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}