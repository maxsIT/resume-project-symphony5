<?php

namespace App\Controller;

use App\Repository\ResumeFeedbackRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class StatisticsController extends BaseController
{
    #[Route('/', name: 'statistics_index', methods: ['GET'])]
    public function index(Request $request, ResumeFeedbackRepository $resumeFeedbackRepository): Response
    {
        return $this->render('statistics/index.html.twig', [
            'ratings' => $resumeFeedbackRepository->getRating(),
        ]);
    }
}
