<?php

namespace App\Controller;

use App\Entity\Resume;
use App\Form\ResumeType;
use App\Repository\ResumeRepository;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/resume')]
class ResumeController extends BaseController
{
    #[Route('/', name: 'resume_index', methods: ['GET'])]
    public function index(ResumeRepository $resumeRepository): Response
    {
        return $this->render('resume/index.html.twig', [
            'resumes' => $resumeRepository->findAll(),
        ]);
    }

    #[Route('/create', name: 'resume_create', methods: ['GET', 'POST'])]
    public function create(Request $request, ResumeRepository $resumeRepository, FileUploader $fileUploader): Response
    {
        $resume = new Resume();
        $form = $this->createForm(ResumeType::class, $resume);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $this->isPropertyValid($resume, 'position')) {
            if ($resume->hasFile() && $form->isValid()) {
                $resumeFile = $form->get('fileName')->getData();
                if ($resumeFile) {
                    $fileName = $fileUploader->upload($resumeFile, null);
                    $resume->setFileName($fileName);
                }

                if ($this->isPropertyValid($resume, 'fileName')) {
                        $resumeRepository->save($resume);
                    return $this->redirectToRoute('resume_index', [], Response::HTTP_SEE_OTHER);
                }
            }
            else if (!$resume->hasFile() && $this->isPropertyValid($resume, 'text')) {
                $resumeRepository->save($resume);
                return $this->redirectToRoute('resume_index', [], Response::HTTP_SEE_OTHER);
            }
        }

        $resume->setText("");
        return $this->renderForm('resume/create.html.twig', [
            'resume' => $resume,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'resume_details', methods: ['GET'])]
    public function show(Resume $resume): Response
    {
        return $this->render('resume/details.html.twig', [
            'resume' => $resume,
        ]);
    }

    #[Route('/{id}/edit', name: 'resume_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Resume $resume, ResumeRepository $resumeRepository, FileUploader $fileUploader): Response
    {
        $form = $this->createForm(ResumeType::class, $resume);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $this->isPropertyValid($resume, 'position')) {
            if ($resume->hasFile() && $form->isValid()) {
                $resumeFile = $form->get('fileName')->getData();
                if ($resumeFile) {
                    $fileName = $fileUploader->upload($resumeFile, $resume->getFileName());
                    $resume->setFileName($fileName);
                }
                
                if ($this->isPropertyValid($resume, 'fileName'))
                {
                    $resumeRepository->save($resume);
                    return $this->redirectToRoute('resume_index', [], Response::HTTP_SEE_OTHER);
                }
            }
            else if (!$resume->hasFile() && $this->isPropertyValid($resume, 'text'))
            {
                $resumeRepository->save($resume);
                return $this->redirectToRoute('resume_index', [], Response::HTTP_SEE_OTHER);
            }
        }

        $resume->setText("");
        return $this->renderForm('resume/edit.html.twig', [
            'resume' => $resume,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'resume_delete', methods: ['POST'])]
    public function delete(Request $request, Resume $resume, ResumeRepository $resumeRepository, FileUploader $fileUploader): Response
    {
        if ($this->isCsrfTokenValid('delete'.$resume->getId(), $request->request->get('_token'))) {
            $fileUploader->delete($resume->getFileName());
            $resumeRepository->remove($resume);
        }

        return $this->redirectToRoute('resume_index', [], Response::HTTP_SEE_OTHER);
    }
}
