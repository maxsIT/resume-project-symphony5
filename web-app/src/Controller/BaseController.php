<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class BaseController extends AbstractController
{
	private ValidatorInterface $validator;

	public function __construct(ValidatorInterface $validator)
	{
		$this->validator = $validator;
	}

	public function isModelValid(mixed $entity): bool
	{
		$errors = $this->validator->validate($entity);
		return count($errors) == 0;
	}

    public function isPropertyValid(mixed $entity, string $fieldName): bool
    {
        $errors = $this->validator->validateProperty($entity, $fieldName);
        return count($errors) == 0;
    }
}
