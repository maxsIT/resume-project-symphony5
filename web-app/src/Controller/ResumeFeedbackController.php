<?php

namespace App\Controller;

use App\Entity\ResumeFeedback;
use App\Form\ResumeFeedbackType;
use App\Repository\CompanyRepository;
use App\Repository\ResumeFeedbackRepository;
use App\Repository\ResumeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/resume-feedback')]
class ResumeFeedbackController extends BaseController
{
    #[Route('/', name: 'resume_feedback_index', methods: ['GET'])]
    public function index(ResumeFeedbackRepository $resumeFeedbackRepository): Response
    {
        return $this->render('resume-feedback/index.html.twig', [
            'resumeFeedbacks' => $resumeFeedbackRepository->findAll(),
        ]);
    }

    #[Route('/create/{resumeId}', defaults: ["resumeId" => 0], name: 'resume_feedback_create', methods: ['GET', 'POST'])]
    public function create(Request $request, $resumeId, ResumeFeedbackRepository $resumeFeedbackRepository,
        CompanyRepository $companyRepository, ResumeRepository $resumeRepository): Response
    {
        $resumeFeedback = new ResumeFeedback();
        $resumeFeedback->setDate(new \DateTime());
        $form = $this->createForm(ResumeFeedbackType::class, $resumeFeedback, [
            'companies' => $companyRepository->findAll(),
            'resumes' => $resumeRepository->findAll(),
            'selectedResumeId' => $resumeId,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $companyId = $form->get('companyId')->getData();
            $resumeId = $form->get('resumeId')->getData();
            if ($companyId == null || $resumeId == null) {
                return $this->renderForm('resume-feedback/create.html.twig', [
                    'resumeFeedback' => $resumeFeedback,
                    'form' => $form,
                    'error' => "Company and resume are required",
                ]);
            }
            
            if (!$resumeFeedbackRepository->isUnique($companyId, $resumeId, $resumeFeedback)) {
                return $this->renderForm('resume-feedback/create.html.twig', [
                    'resumeFeedback' => $resumeFeedback,
                    'form' => $form,
                    'error' => "Feedback from this company for this resume has been already added",
                ]);
            }
            else {
                $company = $companyRepository->find($companyId);
                $resumeFeedback->setCompany($company);
                
                $resume = $resumeRepository->find($resumeId);
                $resumeFeedback->setResume($resume);

                $resumeFeedbackRepository->save($resumeFeedback);
                return $this->redirectToRoute('resume_feedback_index', [], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->renderForm('resume-feedback/create.html.twig', [
            'resumeFeedback' => $resumeFeedback,
            'form' => $form,
            'error' => "",
        ]);
    }

    #[Route('/{id}', name: 'resume_feedback_details', methods: ['GET'])]
    public function show(ResumeFeedback $resumeFeedback): Response
    {
        return $this->render('resume-feedback/details.html.twig', [
            'resumeFeedback' => $resumeFeedback,
        ]);
    }

    #[Route('/{id}/edit', name: 'resume_feedback_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ResumeFeedback $resumeFeedback, ResumeFeedbackRepository $resumeFeedbackRepository,
        CompanyRepository $companyRepository, ResumeRepository $resumeRepository): Response
    {
        $form = $this->createForm(ResumeFeedbackType::class, $resumeFeedback, [
            'companies' => $companyRepository->findAll(),
            'resumes' => $resumeRepository->findAll(),
            'selectedCompanyId' => $resumeFeedback->getCompany()->getId(),
            'selectedResumeId' => $resumeFeedback->getResume()->getId(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            
            $companyId = $form->get('companyId')->getData();
            $resumeId = $form->get('resumeId')->getData();
            if ($companyId == null || $resumeId == null) {
                return $this->renderForm('resume-feedback/edit.html.twig', [
                    'resumeFeedback' => $resumeFeedback,
                    'form' => $form,
                    'error' => "Company and resume are required",
                ]);
            }
            
            if (!$resumeFeedbackRepository->isUnique($companyId, $resumeId, $resumeFeedback)) {
                return $this->renderForm('resume-feedback/edit.html.twig', [
                    'resumeFeedback' => $resumeFeedback,
                    'form' => $form,
                    'error' => "Feedback from this company for this resume has been already added",
                ]);
            }
            else {
                $company = $companyRepository->find($companyId);
                $resumeFeedback->setCompany($company);
                
                $resume = $resumeRepository->find($resumeId);
                $resumeFeedback->setResume($resume);

                $resumeFeedbackRepository->save($resumeFeedback);
                return $this->redirectToRoute('resume_feedback_index', [], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->renderForm('resume-feedback/edit.html.twig', [
            'resumeFeedback' => $resumeFeedback,
            'form' => $form,
            'error' => "",
        ]);
    }

    #[Route('/{id}', name: 'resume_feedback_delete', methods: ['POST'])]
    public function delete(Request $request, ResumeFeedback $resumeFeedback, ResumeFeedbackRepository $resumeFeedbackRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$resumeFeedback->getId(), $request->request->get('_token'))) {
            $resumeFeedbackRepository->remove($resumeFeedback);
        }

        return $this->redirectToRoute('resume_feedback_index', [], Response::HTTP_SEE_OTHER);
    }
}
