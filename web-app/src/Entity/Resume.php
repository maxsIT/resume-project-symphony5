<?php

namespace App\Entity;

use App\Repository\ResumeRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ResumeRepository::class)]
class Resume
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER)]
    private $id;

    #[ORM\Column(length: 128)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 128, maxMessage: 'Position should be shorter than {{ limit }} characters')]
    private $position;

    #[ORM\Column(name: "has_file", type: Types::BOOLEAN)]
    private $hasFile;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Assert\NotBlank(message: 'Resume text is required')]
    private $text = null;

    #[ORM\Column(name: "file_name", length: 32, nullable: true)]
    private $fileName;

    #[Gedmo\Timestampable(on: "create")]
    #[ORM\Column(name: "created_at", type: Types::DATETIME_MUTABLE)]
    private $createdAt;

    #[Gedmo\Timestampable(on: "update")]
    #[ORM\Column(name: "updated_at", type: Types::DATETIME_MUTABLE)]
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function hasFile(): ?bool
    {
        return $this->hasFile;
    }

    public function setHasFile(bool $hasFile): self
    {
        $this->hasFile = $hasFile;

        return $this;
    }

    public function getText(): ?string
    {
        return html_entity_decode(htmlspecialchars_decode($this->text));
    }

    public function setText(?string $text): self
    {
        $this->text = htmlentities(htmlspecialchars($text));

        return $this;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function setFileName(?string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }
}
