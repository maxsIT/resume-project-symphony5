<?php

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: CompanyRepository::class)]
#[UniqueEntity('name', message: 'Company with this name has been already created')]
class Company
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER)]
    private $id;

    #[ORM\Column(name: 'name', length: 128, unique: true)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 128, maxMessage: 'Name should be shorter than {{ limit }} characters')]
    private $name;

    #[ORM\Column(length: 128)]
    #[Assert\NotBlank]
    #[Assert\Url]
    #[Assert\Length(max: 128, maxMessage: 'Website URL should be shorter than {{ limit }} characters')]
    private $website;

    #[ORM\Column(length: 256)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 256, maxMessage: 'Address should be shorter than {{ limit }} characters')]
    private $address;

    #[ORM\Column(length: 16)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 16, maxMessage: 'Phone should be shorter than {{ limit }} characters')]
    #[Assert\Regex(
        pattern: '/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/'
    )]
    private $phone;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }
}
