<?php

namespace App\Form;

use App\Entity\Resume;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use KMS\FroalaEditorBundle\Form\Type\FroalaEditorType;

class ResumeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('position', TextType::class, [
                'attr' => [
                    'placeholder' => 'Position name',
                    'maxLength' => 128
                ]
            ])
            ->add('hasFile', CheckboxType::class, [
                'required' => false,
                'label' => 'Upload resume file',
            ])
            ->add('text', FroalaEditorType::class, [
                'label' => 'Resume text'
            ])
            ->add('fileName', FileType::class, [
                'label' => 'Resume file',
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '25M',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/msword',
                            'application/vnd.openxmlformats-officedocument.wordprocessingml.template'
                        ],
                        'mimeTypesMessage' => 'Only PDF, DOC and DOCX formats are supported for the resume document',
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Resume::class,
        ]);
    }
}
