<?php

namespace App\Form;

use App\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'placeholder' => 'Company name',
                    'maxLength' => 128
                ]
            ])
            ->add('website', UrlType::class, [
                'attr' => [
                    'placeholder' => 'Website URL',
                    'maxLength' => 128,
                ]
            ])
            ->add('address', TextType::class, [
                'attr' => [
                    'placeholder' => 'Company address',
                    'maxLength' => 256,
                ]
            ])
            ->add('phone', TelType::class, [
                'attr' => [
                    'placeholder' => '+38(098)0123456',
                    'maxLength' => 16,
                    'pattern' => '^\+?(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/$',
                    'pattern-error-message' => 'Incorrect phone number format'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
