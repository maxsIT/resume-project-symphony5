<?php

namespace App\Form;

use App\Entity\ResumeFeedback;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResumeFeedbackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $companyChoices = [];
        foreach ($options['companies'] as $company) {
            $companyChoices[$company->getName()] = $company->getId();
        }

        $resumeChoices = [];
        foreach ($options['resumes'] as $resume) {
            $resumeId = $resume->getId();
            $resumeChoices[$resume->getPosition()." [id: ".$resumeId."]"] = $resumeId;
        }

        $builder
            ->add('isPositive', ChoiceType::class, [
                'label' => 'Feedback is',
                'choices' => [
                    'Positive' => true,
                    'Negative' => false,
                ]
            ])
            ->add('date', DateTimeType::class, [
                'widget' => 'single_text',
                'input'  => 'datetime',
            ])
            ->add('companyId', ChoiceType::class, [
                'label' => 'Company',
                'choices' => $companyChoices,
                'mapped' => false,
                'data' => $options['selectedCompanyId']
            ])
            ->add('resumeId', ChoiceType::class, [
                'label' => 'Resume',
                'choices' => $resumeChoices,
                'mapped' => false,
                'data' => $options['selectedResumeId']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ResumeFeedback::class,
            'companies' => [],
            'resumes' => [],
            'selectedCompanyId' => null,
            'selectedResumeId' => null,
        ]);
    }
}
