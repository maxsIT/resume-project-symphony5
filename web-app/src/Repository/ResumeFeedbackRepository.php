<?php

namespace App\Repository;

use App\Entity\ResumeFeedback;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

class ResumeFeedbackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResumeFeedback::class);
    }

    public function save(ResumeFeedback $entity): void
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }

    public function remove(ResumeFeedback $entity): void
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }

    public function isUnique(string $companyId, string $resumeId, ResumeFeedback $resumeFeedback) : bool
    {
        if ($resumeFeedback->getCompany()?->getId() == $companyId && $resumeFeedback->getResume()?->getId() == $resumeId) {
            return true;
        }

        return $this->createQueryBuilder('r')
           ->andWhere('r.company = :companyId and r.resume = :resumeId')
           ->setParameter('companyId', $companyId)
           ->setParameter('resumeId', $resumeId)
           ->getQuery()
           ->getOneOrNullResult() == null;
    }

    public function getRating()
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('r.id, r.position, sum(f.isPositive) as sum')
            ->from('App\Entity\ResumeFeedback', 'f')
            ->leftJoin('App\Entity\Resume', 'r', Join::WITH, 'f.resume = r.id')
            ->addGroupBy('r.id')
            ->orderBy('sum', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
