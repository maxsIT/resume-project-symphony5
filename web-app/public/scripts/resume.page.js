document.addEventListener('DOMContentLoaded', function() {
  document.getElementById('resume_hasFile').onclick = function (e) {
    handleUploadTypeChange(e.target.checked);
  }

  var hasFile = document.getElementById('resume_hasFile').checked;
  handleUploadTypeChange(hasFile);
});

function handleUploadTypeChange (isSelected) {
  var fileInput = document.getElementById('file-input');
  var froalaArea = document.getElementById('froala-area');

  if (isSelected) {
    fileInput.removeAttribute('hidden');
    froalaArea.setAttribute('hidden', 'hidden');
  } else {
    fileInput.setAttribute('hidden', 'hidden');
    froalaArea.removeAttribute('hidden');
  }
}